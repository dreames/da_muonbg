#ifndef DA_MuonBGEVENT_H
#define DA_MuonBGEVENT_H

#include "EventBase.h"

#include <string>
#include <vector>

#include "rqlibProjectHeaders.h"

class DA_MuonBGEvent : public EventBase {

public:
    DA_MuonBGEvent(EventBase* eventBase);
    virtual ~DA_MuonBGEvent();

    TTreeReaderValue<unsigned long> triggerTimeStamp_s;
    TTreeReaderValue<unsigned long> triggerTimeStamp_ns;
    TTreeReaderValue<unsigned long> runID;
    TTreeReaderValue<unsigned long> eventID;
    TTreeReaderValue<float> preTriggerWindow_ms;
    TTreeReaderValue<float> postTriggerWindow_ms;

    TTreeReaderValue<int> nPulses_tpc;
    TTreeReaderValue<float> maxPulseArea_tpc;
    TTreeReaderValue<float> totalPulseArea_tpc;
    TTreeReaderValue<vector<float>> pulseArea_tpc;
    TTreeReaderValue<vector<int>> pulseIDsByArea;
    TTreeReaderValue<vector<string>> classification;
    TTreeReaderValue<vector<int>> pulseStartTime_ns_tpc;
    TTreeReaderValue<vector<int>> peakTime_ns_tpc;
    TTreeReaderValue<vector<float>> topBottomAsymmetry;
    TTreeReaderValue<vector<float>> topArea_phd;
    TTreeReaderValue<vector<float>> botArea_phd;
    TTreeReaderValue<float> totalSpeArea;
    TTreeReaderValue<vector<int>> aft95;
    TTreeReaderValue<vector<int>> aft5;

    TTreeReaderValue<int> maxPulseID_od;
    TTreeReaderValue<float> maxPulseArea_od;
    TTreeReaderValue<vector<int>> coincidence_od;
    TTreeReaderValue<vector<float>> peakAmp_od;
    TTreeReaderValue<vector<float>> pulseZPosition_od;
    TTreeReaderValue<vector<vector<int>>> chID_od;
    TTreeReaderValue<vector<vector<float>>> chPulseArea_od;
    TTreeReaderValue<vector<int>> pulseStartTime_ns_od;
    TTreeReaderValue<vector<int>> peakTime_ns_od;
    
    TTreeReaderValue<int> maxPulseID_skin;
    TTreeReaderValue<float> maxPulseArea_skin;
    TTreeReaderValue<vector<float>> peakAmp_skin;
    TTreeReaderValue<int> maxCoincidence_skin;
    TTreeReaderValue<vector<int>> pulseStartTime_ns_skin;
    TTreeReaderValue<vector<int>> peakTime_ns_skin;

private:
};

#endif // DA_MuonBGEVENT_H
