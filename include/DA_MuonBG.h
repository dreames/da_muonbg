#ifndef DA_MuonBG_H
#define DA_MuonBG_H

#include "Analysis.h"

#include "CutsDA_MuonBG.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

#include "DA_MuonBGEvent.h"

class SkimSvc;

class DA_MuonBG : public Analysis {

public:
    DA_MuonBG();
    ~DA_MuonBG();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsDA_MuonBG* m_cutsDA_MuonBG;
    ConfigSvc* m_conf;
    DA_MuonBGEvent* m_evt;

    float logTpcTotalAreaCut;
    float logTpcAreaLowerLimit;

    float muonHoldoff;
    long muonTime_s;
    int muonTailArea;
    float muonTailLivetime;
    string varNames;

    // array for saving variables to write to TTree
    double varArray[100];

    void FillMuonRateHists();
    void FillSkinAreaVsDeltaTHist();
    void FillTpcTotalAreaVsDeltaTHist();
    void FillTpcMaxAreaVsDeltaTHist();
    void FillOdAreaVsDeltaTHist();
    void FillTbaVsDeltaTHist();
    void FillOdSkinCoincVsDeltaTHist();
    int FindMuonS1();
};

#endif
