#ifndef CutsDA_MuonBG_H
#define CutsDA_MuonBG_H

#include "EventBase.h"
#include "DA_MuonBGEvent.h"

class CutsDA_MuonBG {

public:
    CutsDA_MuonBG(DA_MuonBGEvent* DA_MuonBGEvent);
    ~CutsDA_MuonBG();
    bool IsMuon_all();
    bool IsMuon_od();
    float TBA_od();
    bool IsMuon_skin();
    bool IsMuon_tpc();
    int OdSkinPulseTime_Peak();

private:
    DA_MuonBGEvent* m_evt;   
    ConfigSvc* m_conf;
    int minCoincidence_od;
    double minArea_od;
    double minWidth_od;
    double minAmp_skin;
    double minAreaLog_tpc;
    double minAreaLog_skin;
    vector<float> odPMTZ;
};

#endif
