#include "CutsDA_MuonBG.h"
#include "ConfigSvc.h"
#include "TMath.h"

CutsDA_MuonBG::CutsDA_MuonBG(DA_MuonBGEvent* DA_MuonBGEvent)
{
    m_evt = DA_MuonBGEvent;
    m_conf = ConfigSvc::Instance();
    minCoincidence_od    = m_conf->configFileVarMap["od_coincidence"];
    minArea_od           = m_conf->configFileVarMap["od_area"];
    minWidth_od          = m_conf->configFileVarMap["od_pw"];
    minAmp_skin          = m_conf->configFileVarMap["skin_minAmp"];
    minAreaLog_tpc       = m_conf->configFileVarMap["tpc_logMaxPulseArea"];
    minAreaLog_skin      = m_conf->configFileVarMap["skin_logMinArea"];

    odPMTZ = {2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 2490.46,
              2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 
              2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 2490.46, 1790.46,
              1790.46, 1790.46, 1790.46, 1790.46, 1790.46, 1790.46, 1790.46,
              1790.46, 1790.46, 1790.46, 1790.46, 1790.46, 1790.46, 1790.46,
              1790.46, 1790.46, 1790.46, 1790.46, 1790.46, 1090.46, 1090.46,
              1090.46, 1090.46, 1090.46, 1090.46, 1090.46, 1090.46, 1090.46,
              1090.46, 1090.46, 1090.46, 1090.46, 1090.46, 1090.46, 1090.46,
              1090.46, 1090.46, 1090.46, 1090.46, 390.46, 390.46, 390.46, 390.46,
              390.46, 390.46, 390.46, 390.46, 390.46, 390.46, 390.46, 390.46,
              390.46, 390.46, 390.46, 390.46, 390.46, 390.46, 390.46, 390.46,
              -309.54, -309.54, -309.54, -309.54, -309.54, -309.54, -309.54,
              -309.54, -309.54, -309.54, -309.54, -309.54, -309.54, -309.54,
              -309.54, -309.54, -309.54, -309.54, -309.54, -309.54, -1009.54,
              -1009.54, -1009.54, -1009.54, -1009.54, -1009.54, -1009.54,
              -1009.54, -1009.54, -1009.54, -1009.54,  -1009.54, -1009.54,
              -1009.54, -1009.54, -1009.54, -1009.54, -1009.54, -1009.54, 
              -1009.54, -1009.54};

}

CutsDA_MuonBG::~CutsDA_MuonBG()
{
}

bool CutsDA_MuonBG::IsMuon_all()
{
    // Passes all muon cuts (skin, OD, TPC)
    return IsMuon_od() && IsMuon_skin() && IsMuon_tpc();
}

bool CutsDA_MuonBG::IsMuon_od()
{
    int maxPulseID = *m_evt->maxPulseID_od;
    float maxPulseArea = *m_evt->maxPulseArea_od;
    int maxPulseCoinc = (*m_evt->coincidence_od)[maxPulseID];
    float maxPulseAmp = (*m_evt->peakAmp_od)[maxPulseID];
    float maxPulseWidth = maxPulseAmp/maxPulseArea;
    bool coincOK = maxPulseCoinc > minCoincidence_od;
    bool pulseAreaOK = maxPulseArea > minArea_od;
    bool pulseWidthOK = maxPulseWidth > minWidth_od;
    //bool tbaOK = TBA_od() > 0;
    //return coincOK && pulseAreaOK && pulseWidthOK && tbaOK;
    return coincOK && pulseAreaOK && pulseWidthOK;
}

float CutsDA_MuonBG::TBA_od()
{
    int maxPulseID = *m_evt->maxPulseID_od;
    float centroidZ = -70.*(*m_evt->pulseZPosition_od)[maxPulseID] + 319.046;
    float centroidZcorr = 4.67*centroidZ - 299.40;

    float topArea = 0.;
    float bottomArea = 0.;
    vector<int> chID_vec = (*m_evt->chID_od)[maxPulseID];
    for (int ch = 0; ch < chID_vec.size(); ch++) {
        float chZ = odPMTZ[chID_vec[ch] - 800];
        float chZcorr = 4.67*chZ - 299.40;
        if ((chZcorr - centroidZ) > 740.46) {
            topArea += (*m_evt->chPulseArea_od)[maxPulseID][ch];
        } else {
            bottomArea += (*m_evt->chPulseArea_od)[maxPulseID][ch];
        }
    }
    float tba = (topArea - bottomArea)/(topArea + bottomArea);
    return tba;
}


bool CutsDA_MuonBG::IsMuon_skin()
{
    /*
    int maxPulseID = *m_evt->maxPulseID_skin;
    if (maxPulseID > -1){
        float maxPulseAmp = (*m_evt->peakAmp_skin)[maxPulseID];
        return maxPulseAmp > minAmp_skin;
    }
    return false;
    */
    float maxPulseArea = *m_evt->maxPulseArea_skin;
    if (maxPulseArea <= 0) { return false; }
    return TMath::Log10(maxPulseArea) > minAreaLog_skin;
}

bool CutsDA_MuonBG::IsMuon_tpc()
{
    return TMath::Log10(*m_evt->maxPulseArea_tpc) > minAreaLog_tpc;
}

int CutsDA_MuonBG::OdSkinPulseTime_Peak()
{
    int maxPulseID_od = *m_evt->maxPulseID_od;
    int maxPulseID_skin = *m_evt->maxPulseID_skin;
    int maxPulseStartTime_od = (*m_evt->pulseStartTime_ns_od)[maxPulseID_od];
    int maxPulseStartTime_skin = (*m_evt->pulseStartTime_ns_skin)[maxPulseID_skin];
    // maxPulsePeakTime is relative to maxPulseStartTime
    int maxPulsePeakTime_od = (*m_evt->peakTime_ns_od)[maxPulseID_od];
    int maxPulsePeakTime_skin = (*m_evt->peakTime_ns_skin)[maxPulseID_skin];
    return (maxPulseStartTime_od + maxPulsePeakTime_od) - (maxPulseStartTime_skin + 
            maxPulsePeakTime_skin);
}

