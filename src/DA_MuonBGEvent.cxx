#include "DA_MuonBGEvent.h"

DA_MuonBGEvent::DA_MuonBGEvent(EventBase* base) :
    triggerTimeStamp_s(base->m_reader, "eventHeader.triggerTimeStamp_s"),
    triggerTimeStamp_ns(base->m_reader, "eventHeader.triggerTimeStamp_ns"),
    runID(base->m_reader, "eventHeader.runID"),
    eventID(base->m_reader, "eventHeader.eventID"),
    preTriggerWindow_ms(base->m_reader, "eventHeader.preTriggerWindow_ms"),
    postTriggerWindow_ms(base->m_reader, "eventHeader.postTriggerWindow_ms"),
    
    nPulses_tpc(base->m_reader, "pulsesTPC.nPulses"),
    maxPulseArea_tpc(base->m_reader, "pulsesTPC.maxPulseArea_phd"),
    totalPulseArea_tpc(base->m_reader, "pulsesTPC.totalPulseArea_phd"),
    pulseArea_tpc(base->m_reader, "pulsesTPC.pulseArea_phd"),
    pulseIDsByArea(base->m_reader, "pulsesTPC.pulseIDsByArea"),
    classification(base->m_reader, "pulsesTPC.classification"),
    pulseStartTime_ns_tpc(base->m_reader, "pulsesTPC.pulseStartTime_ns"),
    peakTime_ns_tpc(base->m_reader, "pulsesTPC.peakTime_ns"),
    topBottomAsymmetry(base->m_reader, "pulsesTPC.topBottomAsymmetry"),
    topArea_phd(base->m_reader, "pulsesTPC.topArea_phd"),
    botArea_phd(base->m_reader, "pulsesTPC.bottomArea_phd"),
    totalSpeArea(base->m_reader, "pulsesTPC.totalSinglePEArea_phd"),
    aft95(base->m_reader, "pulsesTPC.areaFractionTime95_ns"),
    aft5(base->m_reader, "pulsesTPC.areaFractionTime5_ns"),

    maxPulseID_od(base->m_reader, "pulsesODHG.maxPulseID"),
    maxPulseArea_od(base->m_reader, "pulsesODHG.maxPulseArea_phd"),
    coincidence_od(base->m_reader, "pulsesODHG.coincidence"),
    peakAmp_od(base->m_reader, "pulsesODHG.peakAmp"),
    pulseZPosition_od(base->m_reader, "pulsesODHG.pulseZPosition"),
    chID_od(base->m_reader, "pulsesODHG.chID"),
    chPulseArea_od(base->m_reader, "pulsesODHG.chPulseArea_phd"),
    pulseStartTime_ns_od(base->m_reader, "pulsesODHG.pulseStartTime_ns"),
    peakTime_ns_od(base->m_reader, "pulsesODHG.peakTime_ns"),

    maxPulseID_skin(base->m_reader, "pulsesSkin.maxPulseID"),
    maxPulseArea_skin(base->m_reader, "pulsesSkin.maxPulseArea_phd"),
    peakAmp_skin(base->m_reader, "pulsesSkin.peakAmp"),
    maxCoincidence_skin(base->m_reader, "pulsesSkin.maxCoincidence"),
    pulseStartTime_ns_skin(base->m_reader, "pulsesSkin.pulseStartTime_ns"),
    peakTime_ns_skin(base->m_reader, "pulsesSkin.peakTime_ns")
{
}

DA_MuonBGEvent::~DA_MuonBGEvent()
{
}
