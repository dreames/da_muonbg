#include "DA_MuonBG.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsDA_MuonBG.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "TMath.h"

// Constructor
DA_MuonBG::DA_MuonBG()
    : Analysis()
    , m_evt(new DA_MuonBGEvent(m_event))
{
    m_event->Initialize();

    logging::set_program_name("DA_MuonBG Analysis");

    m_cutsDA_MuonBG = new CutsDA_MuonBG(m_evt);

    m_conf = ConfigSvc::Instance();
}

// Destructor
DA_MuonBG::~DA_MuonBG()
{
    delete m_cutsDA_MuonBG;
}

// Called before event loop
void DA_MuonBG::Initialize()
{
    INFO("Initializing DA_MuonBG Analysis");

    // cut specs
    logTpcTotalAreaCut   = m_conf->configFileVarMap["tpc_logTotalPulseArea"];
    logTpcAreaLowerLimit = m_conf->configFileVarMap["tpc_lowerLimitPulseArea"];

    // muon tagging vars
    muonHoldoff         =  1.; //seconds
    muonTime_s          = -1;
    muonTailArea        =  0.;
    muonTailLivetime    =  0.;

}

// Called once per event
void DA_MuonBG::Execute()
{
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    //long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
/*
    // are we in a muon holdoff window?
    if (muonTime_s > 0 && triggerTime_s >= muonTime_s) {

        if (triggerTime_s > muonTime_s + muonHoldoff) { // is this the end of the window?
            varArray[10] = muonTailArea/muonTailLivetime;
            m_hists->BookFillTree("muon_candidates", varNames, &varArray[0]);
            muonTime_s = -1; // reset holdoff
        
        } else {
            // increment number of pulses and total livetime in the 5 second window
            muonTailArea += *m_evt->totalPulseArea_tpc;
            float eventWindow = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
            muonTailLivetime += eventWindow*1.e-3; // livetime in seconds
        }
   */
    if (muonTime_s > 0 && *m_evt->runID == varArray[0] && *m_evt->eventID - 1 ==
            varArray[1]) {
        varArray[10] = TMath::Log10(*m_evt->totalPulseArea_tpc);
        m_hists->BookFillTree("muon_candidates", varNames, &varArray[0]);

    } else { // check if this event is a muon
        float logTpcTotalArea = TMath::Log10(*m_evt->totalPulseArea_tpc);
        int muonS1id = FindMuonS1();
                
        // current OD & skin cuts
        if (m_cutsDA_MuonBG->IsMuon_od() && m_cutsDA_MuonBG->IsMuon_skin() && 
                logTpcTotalArea >= logTpcAreaLowerLimit && muonS1id > 0) {


            // calculate whole event TBA
            float topArea = 0.;
            float botArea = 0.;
            for (int i = 0; i < *m_evt->nPulses_tpc; i++) {
                topArea += (*m_evt->topArea_phd)[i];
                botArea += (*m_evt->botArea_phd)[i];
            }

            varNames = "run:event:logTpcArea:logS1area:logOdArea:logSkinArea:odSkinTimeDiff:eventTBA:S1tba:S1pw:nextArea";

            varArray[0]      = *m_evt->runID;
            varArray[1]      = *m_evt->eventID;
            varArray[2]      = logTpcTotalArea;
            varArray[3]      = TMath::Log10((*m_evt->pulseArea_tpc)[muonS1id]);
            varArray[4]      = TMath::Log10(*m_evt->maxPulseArea_od);
            varArray[5]      = TMath::Log10(*m_evt->maxPulseArea_skin);
            varArray[6]      = m_cutsDA_MuonBG->OdSkinPulseTime_Peak();
            varArray[7]      = (topArea - botArea)/(topArea + botArea);
            varArray[8]      = (*m_evt->topBottomAsymmetry)[muonS1id];
            varArray[9]      = (*m_evt->aft95)[muonS1id] - (*m_evt->aft5)[muonS1id];
            varArray[10]     = 0;
            muonTime_s       = triggerTime_s;
            muonTailArea     = 0.;
            muonTailLivetime = 0.;
        }
    }
}


// Called after event loop
void DA_MuonBG::Finalize()
{
}

int DA_MuonBG::FindMuonS1()
{
    vector<int> pulsesByArea = *m_evt->pulseIDsByArea;
    vector<float> pulseAreas = *m_evt->pulseArea_tpc;
    vector<int> peakTimes_tpc = *m_evt->peakTime_ns_tpc;
    int peakTime_od = (*m_evt->peakTime_ns_od)[*m_evt->maxPulseID_od];
    for (int i = 0; i < pulsesByArea.size(); i++) {
        int pulseID = pulsesByArea[i];
        int peakTime_tpc = peakTimes_tpc[pulseID];
        float pulseArea = pulseAreas[pulseID];
        if (pulseArea < 1.e4) { break; }
        if (peakTime_tpc - peakTime_od < 500 || peakTime_od - peakTime_tpc < 500){
            return pulseID;
        }
    }
    return -1;
}



/*
void DA_MuonBG::FillMuonRateHists()
{
    // Fill histograms for rate vs time since muon event
    
    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    string muonID = to_string(muonRunNo) + "_" + to_string(muonEventNo);
    
    // time elapsed between muon event trigger and this trigger
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;

    // event window size for livetime calc
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    m_hists->BookFillHist("livetime_" + muonID, nBins, 0., muonHoldoff, eventDeltaT,
            eventWindow_ms*1e-3);

    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed between muon event trigger and this pulse
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = classification + "pulses" + "_" + muonID;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, pulseTime);
        }
    }
}


void DA_MuonBG::FillTpcTotalAreaVsDeltaTHist()
{
    // plot relationship between muon event total area and number of pulses vs time

    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;

    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = "tpcTotalAreaVsDeltaT_" + classification;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, nBins, 4., 8.5,
                    pulseTime, muonLogTotalTpcArea);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_name = "tpcTotalAreaVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_name, nBins, 0., muonHoldoff, nBins, 4., 8.5, 
            eventDeltaT, muonLogTotalTpcArea, eventWindow_ms*1e-3);
}


void DA_MuonBG::FillSkinAreaVsDeltaTHist()
{
    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;
    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = "skinAreaVsDeltaT_" + classification;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, nBins, 0., 6.,
                    pulseTime, muonLogSkinArea);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_name = "skinAreaVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_name, nBins, 0., muonHoldoff, nBins, 0., 6., 
           eventDeltaT, muonLogSkinArea, eventWindow_ms*1.e-3);
}


void DA_MuonBG::FillTpcMaxAreaVsDeltaTHist()
{
    // plot relationship between muon max pusle area in tpc and 
    // number of pulses vs time

    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;

    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = "tpcMaxAreaVsDeltaT_" + classification;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, nBins, 4., 8.5,
                    pulseTime, muonLogMaxTpcArea);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_name = "tpcMaxAreaVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_name, nBins, 0., muonHoldoff, nBins, 4., 8.5, 
            eventDeltaT, muonLogMaxTpcArea, eventWindow_ms*1e-3);
}


void DA_MuonBG::FillOdAreaVsDeltaTHist()
{
    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;
    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = "odAreaVsDeltaT_" + classification;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, nBins, 3., 6.,
                    pulseTime, muonLogOdArea);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_name = "odAreaVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_name, nBins, 0., muonHoldoff, nBins, 3., 6., 
           eventDeltaT, muonLogOdArea, eventWindow_ms*1.e-3);
}

void DA_MuonBG::FillTbaVsDeltaTHist()
{
    // plot relationship between OD TBA and number of pulses vs time
    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;
    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName = "OdTbaVsDeltaT_" + classification;
            m_hists->BookFillHist(histName, nBins, 0., muonHoldoff, nBins, -1., 1.,
                    pulseTime, muonOdTba);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_name = "OdTbaVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_name, nBins, 0., muonHoldoff, nBins, -1., 1., 
           eventDeltaT, muonOdTba, eventWindow_ms*1.e-3);
}


void DA_MuonBG::FillOdSkinCoincVsDeltaTHist()
{
    int nPulses_tpc = *m_evt->nPulses_tpc;
    int nBins = 5000;
    long triggerTime_s = *m_evt->triggerTimeStamp_s;
    long triggerTime_ns = *m_evt->triggerTimeStamp_ns;
    double eventDeltaT = triggerTime_s - muonTime_s + (triggerTime_ns - 
            muonTime_ns)*1.e-9;
    for (int pulseID = 0; pulseID < nPulses_tpc; pulseID++) {
        string classification = (*m_evt->classification)[pulseID];
        if (classification == "SE" || classification == "S2" || 
                classification == "SPE") {
            // time elapsed since muon trigger
            int pulseTimeRel_ns = (*m_evt->pulseStartTime_ns_tpc)[pulseID];
            double pulseTime = eventDeltaT + pulseTimeRel_ns*1.e-9;
            string histName_peak = "odSkinPeakTimeVsDeltaT_" + classification;
            m_hists->BookFillHist(histName_peak, nBins, 0., muonHoldoff, 200, 0., 200.,
                    pulseTime, muonOdSkinPeakTime);
            string histName_aft1 = "odSkinAft1TimeVsDeltaT_" + classification;
            m_hists->BookFillHist(histName_aft1, nBins, 0., muonHoldoff, 200, 0., 200.,
                    pulseTime, muonOdSkinAft1Time);
        }
    }
    // livetime calculation
    float eventWindow_ms = *m_evt->preTriggerWindow_ms + *m_evt->postTriggerWindow_ms;
    string livetime_peak_name = "odSkinPeakTimeVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_peak_name, nBins, 0., muonHoldoff, 200, 0., 200.,
           eventDeltaT, muonOdSkinPeakTime, eventWindow_ms*1.e-3);
    string livetime_aft1_name = "odSkinAft1TimeVsDeltaT_livetime_s";
    m_hists->BookFillHist(livetime_aft1_name, nBins, 0., muonHoldoff, 200, 0., 200.,
           eventDeltaT, muonOdSkinAft1Time, eventWindow_ms*1.e-3);
}

*/
